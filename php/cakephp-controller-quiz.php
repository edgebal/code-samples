<?php

/**
 *
 * This controller was part of the gamification engine
 * for a sweepstakes-type contest.
 *
 * It handles the Quiz gamification part.
 *
 */

class QuestionsController extends AppController {

	public $uses = array('QuestionSet');

	public function index() {

		/* Get current platform */
		$self = $this;
		$platform = $this->detectPlatform(function() use ($self) {

			/* In case there's no platform */
			$self->redirect(array(
				'controller' => 'users',
				'action' => 'index'
			));

		});

		$user = $this->Session->read('currentUserId');

		if (empty($user)) {
			$this->redirect(array(
				'controller' => 'users',
				'action' => 'index'
			));
		}

		/* */

		$perPage = 4;
		$page = $this->request->query('page') ? (int) $this->request->query('page') : 1;

		if ($page < 1) {
			$page = 1;
		}

		$this->set('cpage', $page);

		$offset = ($page - 1) * $perPage;

		$thisDay = $this->currentDay();

		$this->QuestionSet->Behaviors->attach('Containable');
		$questions = $this->QuestionSet->find('all', array(
			'conditions' => array(
				'QuestionSet.day <=' => $thisDay
			),
			'order' => array('QuestionSet.day' => 'DESC'),
			'contain' => array(
				'Question' => array(
					'id',
					'title'
				)
			),
			'limit' => $perPage,
			'offset' => $offset
		));

		/**/

		$questionsCount = $this->QuestionSet->find('count', array(
			'recursive' => -1,
			'conditions' => array(
				'QuestionSet.day <=' => $thisDay
			)
		));


		$this->set('pages', ceil($questionsCount / $perPage));

		/**/

		$questionSets = Hash::combine($questions, '{n}.Question.{n}.id', '{n}.Question.{n}.question_set_id');

		Configure::write('Export.questionSets', $questionSets);

		$questionIds = Hash::extract($questions, '{n}.Question.{n}.id');

		$this->loadModel('Answer');

		$this->Answer->virtualFields['answer_count'] = 'COUNT(Answer.question_id)';
		$results = Hash::combine($this->Answer->find('all', array(
			'recursive' => -1,
			'fields' => 'Answer.question_id, Answer.answer_count',
			'conditions' => array(
				'Answer.question_id' => $questionIds
			),
			'group' => 'Answer.question_id'
		)), '{n}.Answer.question_id', '{n}.Answer.answer_count');
		unset($this->Answer->virtualFields['answer_count']);

		Configure::write('Export.dailyQuestionResults', $results);

		$user_questions = Hash::extract($this->Answer->find('all', array(
			'recursive' => -1,
			'conditions' => array(
				'Answer.user_id' => $user,
				'Answer.question_id' => $questionIds
			)
		)), '{n}.Answer.question_id');

		Configure::write('Export.userQuestions', $user_questions);

		$user_sets = array();

		foreach ($user_questions as $user_question) {
			$user_sets[] = $questionSets[$user_question];
		}

		$this->set('userSets', $user_sets);
		Configure::write('Export.userSets', $user_sets);

		$this->set('questions', $questions);

	}

	public function ajax_answer() {

		$this->autoRender = false;

		$user = $this->Session->read('currentUserId');

		if (empty($user)) {
			throw new ForbiddenException;
		}

		if (!$this->request->data('id')) {
			throw new BadRequest;
		}

		$id = (int) $this->request->data('id');

		$this->loadModel('Ballot');
		$this->loadModel('Question');
		$this->loadModel('Answer');

		$done = false;

		$question = $this->Question->find('first', array(
			'recursive' => -1,
			'conditions' => array(
				'id' => $id
			)
		));

		if (empty($question)) {
			throw new BadRequest;
		}

		$setIds = Hash::extract($this->Question->find('all', array(
			'recursive' => -1,
			'conditions' => array(
				'question_set_id' => $question['Question']['question_set_id']
			)
		)), '{n}.Question.id');

		$already = $this->Answer->find('count', array(
			'recursive' => -1,
			'conditions' => array(
				'user_id' => $user,
				'question_id' => $setIds
			)
		));

		if (!$already) {

			$this->Answer->create();
			$saved = $this->Answer->save(array(
				'question_id' => $id,
				'user_id' => $user
			));

			if ($saved) {

				$done = true;
				$this->Ballot->grant($user, 'daily_question', 5);

			}

		}


		$ballots = $this->Ballot->myBallots($user);

		echo json_encode(array('done' => $done, 'ballots' => $ballots));

	}

}
