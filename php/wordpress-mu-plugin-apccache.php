<?php namespace Edgebal; defined('ABSPATH') or die;
/**
 * Plugin Name: APC Cache
 * Plugin URI: https://github.com/edgebal/wp-apccache
 * Description: PHP's APC/APCu wrapper for Wordpress. MU plugin.
 * Version: 0.0.1
 * Author: Emiliano Balbuena
 * Author URI: https://github.com/edgebal
 * Network: true
 */

if (class_exists('APCCache')) {
	return;
}

class APCCache {

	private $ttl = 1;

	/* Setter */

	public function set($key, $value = null, $overrideTtl = false) {

		if (empty($key)) {
			throw new Exception('Invalid cache key');
		}

		if (($overrideTtl !== false) && (!is_numeric($overrideTtl) || ($overrideTtl < 0))) {
			throw new Exception('Invalidttl value');

		}

		if ($value === null) {
			return false;
		}

		$ttl = $overrideTtl ?: $this->ttl;

		$stored = apc_store($this->prefix . $key, $value, $ttl);

		return $stored;

	}

	/* Getter */

	public function get($key, $notFoundCallback = null) {

		if (empty($key)) {
			throw new Exception('Invalid cache key');
		}

		$exists = false;
		$value = apc_fetch($this->prefix . $key, $exists);

		if (!$exists && is_callable($notFoundCallback)) {

			$value = call_user_func_array($notFoundCallback, [
				$this,
				$key,
				$this->prefix . $key
			]);

			if ($value !== null) {
				$this->set($key, $value);
			}

		}
		elseif (($notFoundCallback !== null) && !is_callable($notFoundCallback)) {
			throw new Exception('Invalid get() callback');
		}

		return $value;

	}

	/* Constructor */

	public function __construct($id = null, $ttl = null) {

		if (empty($id)) {
			throw new Exception('Invalid APCCache id');
		}

		if (empty($ttl)) {
			throw new Exception('Missing expiration value');
		}

		if (!is_numeric($ttl) || ($ttl <= 0)) {
			throw new Exception('Invalid expiration value');
		}

		$this->ttl = $ttl;
		$this->prefix = $id . '_';

	}

	/* Instance Registry */

	private static $instances = [];

	public static function factory($id = null, $ttl = null) {
		global $wpdb;

		if (empty($id)) {
			throw new Exception('Invalid APCCache id');
		}

		if (!empty($wpdb->prefix)) {
			$id = $wpdb->prefix . $id;
		}

		if (!isset(static::$instances[$id])) {
			static::$instances[$id] = new static($id, $ttl);
		}

		return static::$instances[$id];

	}

}
