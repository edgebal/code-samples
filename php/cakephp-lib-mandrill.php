<?php

/**
 *
 * This Mail Transport acts as a wrapper layer between
 * CakePHP's CakeEmail object and Mandrill (MailChimp's
 * transactional email service)
 *
 */


App::uses('AbstractTransport', 'Network/Email');

class MandrillTransport extends AbstractTransport {

	public function send(CakeEmail $Email) {

		App::import('Vendor', 'Mandrill/Mandrill');

		$config = $Email->config();

		if (empty($config['key'])) {
			throw new SocketException('Missing Mandrill key');
		}

		$mandrill = new Mandrill($config['key']);

		$headers = $Email->getHeaders(array('from', 'sender', 'replyTo', 'readReceipt', 'returnPath', 'to', 'cc', 'bcc'));

		foreach ($Email->from() as $e => $n) {
			$from = $e;
			$fromName = $n;
			break;
		}

		$to = array();

		foreach ($Email->to() as $e => $n) {

			$to[] = array(
				'email' => $e,
				'name' => $n,
				'type' => 'to'
			);

		}

		$body = $Email->message(CakeEmail::MESSAGE_HTML);

		$message = array(
			'html' => $body,
			'subject' => $Email->subject(),
			'from_email' => $from,
			'from_name' => $fromName,
			'to' => $to,
			'track_opens' => !empty($config['track_opens']) ? true : false,
			'track_clicks' => !empty($config['track_clicks']) ? true : false,
			'auto_text' => !empty($config['auto_text']) ? true : false
		);

		$attachments = $Email->attachments();

		if (!empty($attachments)) {

			$message['attachments'] = array();

			foreach ($attachments as $file => $data) {

				$message['attachments'][] = array(
					'type' => $data['mimetype'],
					'name' => $file,
					'content' => base64_encode(file_get_contents($data['file']))
				);

			}

		}

		if (!empty($config['tags'])) {
			$message['tags'] = $config['tags'];
		}

		try {
			$result = $mandrill->messages->send($message, false, Configure::read('Mandrill.ipPool'), false);
		}
		catch(Mandrill_Error $e) {
			$result = array('error' => true, 'msg' => $e->getMessage());
		}

		return $result;

    }

}
