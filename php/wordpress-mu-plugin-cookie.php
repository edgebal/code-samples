<?php namespace Edgebal; defined('ABSPATH') or die;
/**
 * Plugin Name: Cookies
 * Plugin URI: https://github.com/edgebal/wp-cookie
 * Description: Cookie helper for Wordpress. MU plugin.
 * Version: 0.0.1
 * Author: Emiliano Balbuena
 * Author URI: https://github.com/edgebal
 * Network: true
 */

if (class_exists('Cookie')) {
	return;
}

class Cookie {

	public function save($id, $data, $ttl = 0, $secure = false) {

		$prefixedId = $this->id($id);

		if (!is_numeric($ttl)) {
			$ttl = strtotime($ttl);

			if (!$ttl) {
				throw new Exception('Wrong TTL value for cookie');
			}
		}
		else {

			$ttl += time();

		}

		return setcookie($prefixedId, $data, $ttl, COOKIEPATH, COOKIE_DOMAIN, (bool) $secure);

	}

	public function read($id, $notFoundCallback = null) {

		$prefixedId = $this->id($id);

		if (!isset($_COOKIE[$prefixedId])) {

			$value = null;

			if (($notFoundCallback !== null) && is_callable($notFoundCallback)) {

				$value = call_user_func_array($notFoundCallback, [
					$id,
					$prefixedId,
					$this
				]);

			}
			elseif (($notFoundCallback !== null) && !is_callable($notFoundCallback)) {
				throw new Exception('Invalid read() callback');
			}

		}
		else {
			$value = $_COOKIE[$prefixedId];
		}

		return $value;

	}

	public function delete($id) {

		$prefixedId = $this->id($id);

		return setcookie($prefixedId, '', time() - 86400, COOKIEPATH, COOKIE_DOMAIN);

	}

	protected function id($id) {
		global $wpdb;

		if (empty($id)) {
			throw new Exception('Missing cookie id');
		}

		return $wpdb->prefix . $id;
	}

}
