<?php

/**
 *
 * This Shell task was used to
 * send notification emails to contest users
 * on several sweepstakes I built
 *
 */

class CronShell extends AppShell {

	public $uses = array('User', 'Notification');

	/**
	 * !IMPORTANT! THIS SHOULD BE ORDERED FROM LATER TO NEWER IN TIME
	 */
	private $notifications = array(
		'2_weeks_inactive' => array(
			'when' => '-2 weeks',
			'subject' => 'Increase your chances of winning the $20k'
		),
		'3_days_inactive' => array(
			'when' => '-5 days',
			'subject' => 'Still thinking about what you’d reno with $20k?'
		),
	);

	public function main() {

		/* Domain */

		Router::fullBaseUrl(Configure::read('shellDomain'));

		App::uses('CakeEmail', 'Network/Email');

		foreach ($this->notifications as $notification => $options) {

			$this->emailRecipients($notification, $options);

		}

	}

	/* */

	private function emailRecipients($notification, $options) {

		$start = Configure::read('Contest.startDay');

		$result = array();

		if (!empty($options['when'])) {

			$when = strtotime($options['when']);

			if ($when < $start) {
				return;
			}

			$whenDate = date('Y-m-d', $when);

			$conditions = array(
				'User.has_reminders_active' => 1,
				"User.{$notification}" => 0,
				'User.created <' => $whenDate . ' 00:00:00',
				'OR' => array(
					array('User.last_login' => null),
					array('User.last_login <' => $whenDate)
				)
			);

		}
		else {
			return;
		}

		$users = $this->User->find('all', array(
			'recursive' => -1,
			'fields' => 'User.id, User.name, User.email, User.facebook, User.twitter',
			'conditions' => $conditions,
			'limit' => 100, /* 100 users per task */
		));

		$result = Hash::combine($users, '{n}.User.email', '{n}.User');

		/* */

		$flagAsSent = array();
		$notifReverse = array_reverse(array_keys($this->notifications));

		foreach ($notifReverse as $n) {

			$flagAsSent[] = $n;

			if ($n === $notification) {
				break;
			}

		}

		/* */

		$notif_tag = str_replace('_', '-', $notification);

		foreach ($users as $user) {

			$mail_user = $user['User']['id'];
			$mail_user_hash = md5(Configure::read('Security.salt') . $mail_user);


			$unsubs = Router::url(array(
				'controller' => 'users',
				'action' => 'unsubscribe',
				'?' => array(
					'id' => $mail_user,
					'hash' => $mail_user_hash
				)
			), true);


			$Email = new CakeEmail('default');

			$result = $Email->template($notification, 'default')
			->emailFormat('html')
			->viewVars(array(
				'app_link' => Router::url(array(
					'controller' => 'users',
					'action' => 'login',
					'?' => array(
						'platform' => $user['User']['facebook'] ? 'facebook' :
							$user['User']['twitter'] ? 'twitter' :
							null
					)
				), true),
				'unsubs_link' => $unsubs,
				'user_mail' => strtolower($user['User']['email']),
				'app_mail' => implode(', ', array_keys($Email->from())),
				'rnr_app_link' => Router::url(array('controller' => 'pages', 'action' => 'display', 'rnr'), true)
			))
			->to(array($user['User']['email'] => $user['User']['name']))
			->domain(Configure::read('App.fullBaseUrl'))
			->subject($options['subject'])
			->config(array('tags' => explode(',', 'reminder,' . $notif_tag)))
			->send();

			$this->out('Sent email:' . $user['User']['email'] . ' - ' . $notification . "\n");

			$data = array(
				'id' => $user['User']['id'],
				'last_email' => date('Y-m-d')
			);

			foreach ($flagAsSent as $field) {
				$data[$field] = 1;
			}

			$this->User->save($data);

		}

	}

}
