/**
 *
 * SailsJS controller to connect with a
 * Google account.
 *
 * Please, also view: sailsjs-token-model.js
 *
 */

module.exports = {

  status: function(req, res) {

      return Promise.all([
        Token.isConnected(clientName)
      ]).then(function(results) {

        var connected = results[0];

        res.view({
          status: connected ? 'connected' : 'disconnected',
          isConnected: connected
        });

      });

  },

  connect: function(req, res) {

    var url = 'https://accounts.google.com/o/oauth2/auth?';

    url += [
      'scope=' + encodeURIComponent('profile email https://www.googleapis.com/auth/tagmanager.readonly'),
      'state=' + encodeURIComponent(Date.now() + '!010101!'), // TODO: CSRF protection
      'redirect_uri=' + encodeURIComponent(sails.config.google.redirectUri),
      'response_type=code',
      'client_id=' + encodeURIComponent(sails.config.google.clientId),
      'access_type=offline',
      'approval_prompt=force'
    ].join('&');

    res.redirect(url);

  },

  oauth2callback: function(req, res) {

    if (typeof req.query.code === 'undefined') {
      return res.forbidden();
    }

    Token.oauth(clientName, {

      code: req.query.code,
      redirect_uri: sails.config.google.redirectUri,
      grant_type: 'authorization_code'

    }).then(function() {

      res.redirect('/status');

    });

  }

};
