/**
*
* SailsJS model to connect with a
* Google account.
*
* Please, also view: sailsjs-token-controller.js
*
*/

var moment = require('moment');

module.exports = {

  attributes: {
    client: {
      type: 'string',
      size: 30
    },
    refreshToken: {
      type: 'string',
      size: 100
    },
    accessToken: {
      type: 'string',
      size: 100
    },
    expires: {
      type: 'datetime'
    }
  },

  get: function(client) {

    var self = this;

    return this.findOne({
      client: client
    }).then(function(token) {

      if (!token) {
        throw new Error('Missing token');
      }

      var now = moment(),
        expires = moment(token.expires);

      if (now.isBefore(expires)) {
        return token.accessToken;
      }

      return self.oauth(client, {
        refresh_token: token.refreshToken,
        grant_type: 'refresh_token'
      });

    });

  },

  oauth: function(clientName, formData) {

    var self = this;

    var
      request = require('request'),
      moment = require('moment');

    return new Promise(function(resolve, reject) {

      var fullFormData = _.merge({
        client_id: sails.config.google.clientId,
        client_secret: sails.config.google.clientSecret,
      }, formData);

      request.post({
        url: 'https://www.googleapis.com/oauth2/v3/token',
        form: fullFormData
      }, function(err, response, body) {

        if (err) {
          return reject(err);
        }

        var data = JSON.parse(body);

        if (response.statusCode !== 200) {
          reject(data);
          return;
        }

        if (typeof data.access_token === 'undefined') {
          reject(data);
          return;
        }

        var expires = moment().add(data.expires_in, 's').toISOString();

        resolve(self.findOrCreate({
          client: clientName
        }, {
          client: clientName
        }).then(function(token) {

          var newTokenData = {
            accessToken: data.access_token,
            expires: expires
          };

          if (typeof data.refresh_token !== 'undefined') {
            newTokenData.refreshToken = data.refresh_token;
          }

          return self.update({
            client: token.client
          }, newTokenData).then(function() {

            return data.access_token;

          });

        }));

      });

    });

  },

  isConnected: function(client) {

    return this.find({
      client: client
    }).limit(1).then(function(tokens) {

      if (tokens.length == 0) {
        return false;
      }

      var token = tokens[0];

      return !!token.refreshToken;

    });

  },

};
