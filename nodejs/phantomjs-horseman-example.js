/**
 *
 * Partial code for a script
 * that analyzes the tracking tags present on a page
 *
 */

var Horseman = require('node-horseman');

var parserList = sails.config.app.parserList;

var moment = require('moment');

var performPageAnalysis = function(url, client) {

  return new Promise(function(resolve, reject) {

    var horseman = new Horseman(),
      waiter = new Waiter(),
      closed = false,
      parsers = {},
      data = { $url: url, $client: client };

    parserList.forEach(function(className) {
      parsers[className] = sails.services[className.toLowerCase()].factory(horseman);
    });

    var closeAnalyzedPage = function() {

        if (closed) {
          return;
        }

        closed = true;

        horseman.close();
        sails.log('Closed ' + url);

        resolve(data);

    };

    horseman.on('loadStarted', function() {

      sails.log('Loaded ' + url);

      parserList.forEach(function(className) {
        if (typeof parsers[className].onloadstarted === 'function') {
          parsers[className].onloadstarted();
        }
      });

      setTimeout(closeAnalyzedPage, 30 * 1000);

    });

    horseman.on('loadFinished', function() {

      sails.log('Finished loading ' + url);

      parserList.forEach(function(className) {
        if (typeof parsers[className].onloadfinished === 'function') {
          parsers[className].onloadfinished();
        }
      });

    });

    horseman.on('resourceReceived', function(response) {

      if (response.status !== 200) {
        // TODO: Log failed and/or redirected stuff?
        return;
      }

      parserList.forEach(function(className) {
        if (typeof parsers[className].onresourcechunk === 'function') {
          parsers[className].onresourcechunk(response.url, response);
        }
      });

      if (response.stage !== 'end') {
        return;
      }

      parserList.forEach(function(className) {
        if (typeof parsers[className].onresource === 'function') {
          parsers[className].onresource(response.url);
        }
      });

    });

    horseman.on('error', function(err, trace) {
      sails.log.error(err);
    });

    sails.log('Opening ' + url);
    horseman.viewport(1366, 768).open(url);

    parserList.forEach(function(className) {
      if (typeof parsers[className].onopen === 'function') {
        parsers[className].onopen(waiter);
      }
    });

    sails.log('Finished opening ' + url);

    data.$title = horseman.title();
    data.$url = horseman.url();

    parserList.forEach(function(className) {

      var result = parsers[className].result();

      if (result !== null) {
        data[className] = result;
      }

    });

    setTimeout(closeAnalyzedPage, waiter.get() * 1000);

  });

}
