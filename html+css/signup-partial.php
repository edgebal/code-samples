<?php

//
// Wordpress custom signup screen
//

?>

<?php defined('ABSPATH') or die; ?>

<?php fu_set_page_title('Register'); $sn_verb = 'Sign up'; ?>

<?php get_header(); ?>
<?php get_template_part('elements/site-header'); ?>

<?php $type_bar_title = 'Register'; include __DIR__ . '/partials/type-bar.php' ?>

<div class="user-portal">

	<div class="hero-banner"></div>

	<div class="social-login">

		<div class="social-login-item social-login-facebook">
			<a href="<?= get_site_url(null, '/socialconnect?platform=facebook&next=' . urlencode($next)) ?>"><span class="icon"></span><?= $sn_verb ?> with FACEBOOK</a>
		</div>
		<div class="social-login-item social-login-google">
			<a href="<?= get_site_url(null, '/socialconnect?platform=google&next=' . urlencode($next)) ?>"><span class="icon"></span><?= $sn_verb ?> with GOOGLE+</a>
		</div>

	</div>


	<div class="sep">
		or
		<div class="br"></div>
		<h4>Sign up with email</h4>
	</div>

	<form class="reader-form reader-form-signup" action="<?= fu_reader_url('signup', $next) ?>" method="post" autocomplete="off">

		<div class="field">
			<label for="name">Full Name:</label>
			<input type="text" id="name" name="name" value="<?= request_data('name') ?>">
		</div>
		<div class="field">
			<label for="email">Email address:</label>
			<input type="email" id="email" name="email" value="<?= request_data('email') ?>">
		</div>
		<div class="field">
			<label for="password">Password:</label>
			<input type="password" id="password" name="password" value="<?= request_data('password') ?>">
		</div>
		<div class="field">
			<input class="styled-checkbox-control" type="checkbox" id="agree" name="agree" <?= request_data('agree') ? 'checked="checked"' : '' ?>>
			<label for="agree" class="inline-checkbox">I agree to <a target="_blank" href="<?= get_site_url(null, '/tos') ?>">TOS</a> and <a target="_blank" href="<?= get_site_url(null, '/privacy') ?>">Privacy Policy</a></label>
		</div>
		<div class="field">
			<input class="styled-checkbox-control" type="checkbox" id="optin" name="optin" <?= request_data('optin') ? 'checked="checked"' : '' ?>>
			<label for="optin" class="inline-checkbox">I want to receive email notifications when there's healthy foods that help my condition, overall well being and related content.</label>
		</div>
		<div class="submit">
			<button type="submit" class="btn btn-signup"><span class="icon icon-user-add"></span> Register</button>
		</div>

		<div class="extra">
			<div class="thick-br"><div></div></div>
			<h4>Already registered?</h4>
			<a href="<?= fu_reader_url('login', $next) ?>" class="btn-gray btn-login"><span class="icon icon-key"></span> Login</a>
		</div>

	</form>

</div>

<?php get_template_part('elements/site-footer'); ?>
<?php get_footer(); ?>
