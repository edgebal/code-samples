<?php
	//
	// Partial for page elements that renders a
	// hero-type banner at the top of the page
	//
	// Build flow will convert to CSS (when @extended)
	// and add rem->px fallback declarations
	//
	// Please, also view: _search-hero.scss
	// And: ../javascript/rotating-placeholder.js
	//
?>

<div class="search-hero">

    <h2>
        Lorem ipsum...
    </h2>
    <p>
        Dolor sit amet!
    </p>
    <div class="search">

        <div class="search-box">

            <input type="text" class="search-input" name="search" placeholder="Search" data-rotateplaceholder="<?= htmlspecialchars(json_encode([
                'effect' => 'typewriterErase',
                'duration' => 6,
                'template' => 'Search by $0',
                'items' => [
                    'conditions',
                    'ailments',
                    'stories',
                    'food',
                    'recipes',
                    'meal plans'
                ]
            ])) ?>" />
            <a href="#" class="search-button">Search</a>

        </div>

    </div>

</div>
