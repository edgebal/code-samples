/**
 * This directive allows one or more elements to occupy a
 * % of the available vertical space on the viewport.
 *
 * Example markup:
 * <header vertical-stack="fixed">Fixed height</header>
 * <div vertical-stack="90">90% of available viewport</div>
 * <div vertical-stack="10">10% of available viewport</div>
 * <footer vertical-stack="fixed">Fixed height</footer>
 *
 */

/**
 * Directive: [vertical-stack]
 */

App.directive('verticalStack', ['$window', '$document', function($window, $document) {

	let $w = angular.element($window);

	if (!$window._verticalStack) {
		$window._verticalStack = {
			fixed: [],
			dynamic: []
		}
	}

	let affixElements = function() {

		let stack = $window._verticalStack,
			size = 0,
			height = $w.height(),
			available = 0;

		if (stack.dynamic.length === 0) {
			return;
		}

		stack.fixed.forEach(function($el) {

			size += $el
			.wrap('<div style="border: 1px solid transparent;"></div>')
			.parent().height(); // real height hack
   			$el.unwrap();

		});

		available = height - size;

		if (available <= 0) {
			return;
		}

		stack.dynamic.forEach(function(target) {

			target.el.css('height', available * target.ratio);

			if (target.cloaked && !target.el.data('vsCloakRemoved')) {
				target.el.removeAttr('vertical-stack-cloak');
				target.el.data('vsCloakRemoved', true);
			}

			target.el.trigger('resizeElement');

		});

	};

	let affixElementsIntent = _.throttle(affixElements, 1000 / 12);

	let link = function($scope, el, attr) {

		if (!attr.verticalStack) {
			return;
		}

		if (attr.verticalStack == 'fixed') {
			$window._verticalStack.fixed.push(angular.element(el));
		}
		else {
			$window._verticalStack.dynamic.push({
				el: angular.element(el),
				ratio: parseFloat(attr.verticalStack) / 100,
				cloaked: typeof attr.verticalStackCloak !== 'undefined'
			});
		}

	};

	$w.on('resize', affixElementsIntent);
	window.setTimeout(affixElementsIntent, 50)

	return {
		priority: 1000,
		restrict: 'A',
		link
	};

}]);
