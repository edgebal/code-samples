/**
 * <html> dynamic font-size
 * for rem based designs
 *
 * Example: http://i.imgur.com/CAUph61.gifv
 *
 */

(function(d, win) {

    let h = d.getElementsByTagName('html')[0],
        r = 5.12,
        dEl = d.documentElement;

    if (typeof d.addEventListener === 'undefined') {
        // Browser not supported
        return;
    }

    // begin http://sampsonblog.com/749/simple-throttle-function
    let throttle = function(callback, limit) {
        var wait = false;                     // Initially, we're not waiting
        return function () {                  // We return a throttled function
            if (!wait) {                      // If we're not waiting
                callback.call();              // Execute users function
                wait = true;                  // Prevent future invocations
                win.setTimeout(function () {  // After a period of time
                    wait = false;             // And allow future invocations
                }, limit);
            }
        }
    }
    // end

    let responsiveIt = function() {

        if (dEl.clientWidth > 768) {

                h.style.fontSize = '';

                return;

        }

        let fs;

        if (dEl.clientWidth > dEl.clientHeight) {
            // landscape
            fs = dEl.clientHeight / r;
        }
        else {
            // portrait
            fs = dEl.clientWidth / r;
        }

        h.style.fontSize = `${fs}%`;

    };

    win.addEventListener('DOMContentLoaded', responsiveIt, false);
    win.addEventListener('load', responsiveIt, false);
    win.addEventListener('resize', throttle(responsiveIt, 200), false);

    try {
        responsiveIt();
    }
    catch (e) {
        window.setTimeout(responsiveIt, 500);
    }

})(document, window);
