/**
 * This jQuery-based library allows to animate the text in
 * <input> placeholder attributes.
 *
 * Example: http://i.imgur.com/FFFQ7Ej.gifv
 *
 * Please, also view: ../html+css/search-hero-partial.php for implementation
 *
 */

;(function() {

    // shim layer with setTimeout fallback
    window.requestAnimFrame = (function(){
        return  window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        function( callback ){
            window.setTimeout(callback, 1000 / 30);
        };
    })();

    let testInput = document.createElement('input');

    if (!('placeholder' in testInput)) {
        return;
    }

    let rotateplaceholderEffects = {
        replace: function(item, template, $el, options) {

            let text = template.replace('$0', item);
            $el.attr('placeholder', text);

        },
        typewriterErase: function(item, template, $el, options) {

            let duration = (options.duration || 5) * 1000,
                delay = Math.floor(duration * 0.125),
                writing = Math.floor(duration * 0.25),
                erasing = Math.floor(duration * 0.125),
                erasingOffset = duration - erasing,
                timer = $el.data('tweTimer'),
                zero = Date.now(),
                cursor = '|';

            if (timer) {
                window.clearInterval(timer);
            }

            timer = window.setInterval(function() {

                let now = Date.now(),
                    delta = now - zero,
                    phase = 'done',
                    progress = 1,
                    partialItem;

                if (delta <= delay) {
                    phase = 'waiting';
                }
                else if (delta - delay <= writing) {
                    phase = 'writing';
                    progress = (delta - delay) / writing;
                }
                else if (delta >= erasingOffset) {
                    phase = 'erasing';
                    progress = (delta - erasingOffset) / erasing;
                }

                if (progress > 1) {
                    progress = 1;
                }

                switch (phase) {
                    case 'waiting':

                        partialItem = cursor;

                        break;
                    case 'done':

                        partialItem = item;

                        break;
                    case 'erasing':

                        progress = 1 - progress;

                    case 'writing':

                        let shownChars = Math.ceil(item.length * progress);
                        partialItem = item.substr(0, shownChars) + cursor;

                        break;
                }

                if ($el.data('rphLastText') == partialItem) {
                    return;
                }

                window.requestAnimFrame(function() {
                    $el.attr('placeholder', template.replace('$0', partialItem));
                });

                $el.data('rphLastText', partialItem);

            }, 1000 / 30);

            $el.data('tweTimer', timer);

        }
    };

    let rotateplaceholderMe = function(options) {

        let $el = $(this);

        if (!options.duration) {
            options.duration = 5;
        }

        $el.data('rphOptions', options);

        let process = () => {

            let $el = $(this),
                options = $el.data('rphOptions'),
                count = $el.data('rphCount') || 0,
                items = options.items,
                idx = count % items.length,
                template = options.template,
                item = items[idx],
                effect = options.effect || 'replace';

            rotateplaceholderEffects[effect].call(this, item, template, $el, options);

            $el.data('rphCount', parseInt(idx, 10) + 1);

        };

        let timer = window.setInterval(process, options.duration * 1000);
        process();

        $el.data('rphTimer', timer);

    };

    $(function() {

        $('[data-rotateplaceholder]').each(function() {

            let options = JSON.parse($(this).attr('data-rotateplaceholder'));

            rotateplaceholderMe.call(this, options);

        });

    });

})();
